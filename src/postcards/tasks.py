from celery import Celery
from celery import shared_task
from django.conf import settings
from django.core import mail

from .email_messages.email_postcard import EmailPostcard

app = Celery(settings.CELERY_APP_NAME, backend='amqp',
             broker=settings.CELERY_BROKER_STRING)


@shared_task
def send_postcard_to_email(img_url, sender_name, author_name, message_text, email, style):
    """
    Task that sends a postcard to the email.
    :param img_url: url of the image
    :param sender_name: sender username
    :param author_name: author username
    :param message_text: message body
    :param email: email to send
    :param style: style to append
    :return: None
    """
    subject = 'You have a postcard!'

    # Generating message from template
    email_postcard = EmailPostcard(
        subject='You have a postcard!',
        img_url=img_url,
        sender=sender_name,
        author=author_name,
        text=message_text,
        style=style,
    )

    html_message = email_postcard.html_message
    plain_message = email_postcard.plain_message

    mail.send_mail(
        subject=subject,
        from_email='PostcardMailer',
        message=plain_message,
        recipient_list=[email],
        html_message=html_message
    )
