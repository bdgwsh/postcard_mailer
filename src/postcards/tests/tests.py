import os

from django.conf import settings
from django.contrib.auth.models import User
from django.core import mail
from django.core.files.images import ImageFile
from django.test import TestCase
from postcards.models import Postcard
from postcards.tasks import send_postcard_to_email
from rest_framework import status


class TestPostcards(TestCase):

    def setUp(self):
        from rest_framework.test import APIClient
        self.client = APIClient()
        self.username = 'tester'
        self.password = '123456dasd1d'
        self.email = 'user@domain.com'
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password
        )
        self.client.login(username=self.email, password=self.password)
        self.pic320x100 = os.path.join(settings.BASE_DIR, 'postcards/tests/fixtures/img', '320x100.png')
        self.image = open(self.pic320x100, 'rb')
        self.text = 'text postcard'
        self.postcard = Postcard.objects.create(
            text=self.text,
            author=self.user,
            image=ImageFile(self.image)
        )
        self.jwt_token_url = '/token-auth/jwt/create/'
        self.auth = {
            'username': self.username,
            'password': self.password
        }
        self.access_token = 'Bearer ' + self.client.post(self.jwt_token_url, self.auth).data['access']

        self.url = '/postcards/'

    def tearDown(self):
        self.client.logout()
        self.image.close()

    def test_create_postcard(self):
        pic320x100 = os.path.join(settings.BASE_DIR, 'postcards/tests/fixtures/img', '320x100.png')
        image = open(pic320x100, 'rb')
        data = {
            'text': 'Test Text!',
            'image': image,
        }
        response = self.client.post(self.url, data, format='multipart', HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_create_postcard_fail_size(self):
        pic225x225 = os.path.join(settings.BASE_DIR, 'postcards/tests/fixtures/img', '225x225.png')
        image = open(pic225x225, 'rb')
        data = {
            'text': 'Test Text!',
            'image': image,
        }
        response = self.client.post(self.url, data, format='multipart', HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_send_postcard_to_email(self):
        send_postcard_to_email(
            self.postcard.image.url,
            self.user.username,
            author_name=self.postcard.author.username,
            message_text=self.postcard.text,
            email=['test@gmail.com'],
            style=self.postcard.style
        )
        self.assertEqual(len(mail.outbox), 1)

    def test_delete_postcard(self):
        url = self.url + f'{str(self.postcard.pk)}/'
        response = self.client.delete(url, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
