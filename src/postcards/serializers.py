import textwrap

from rest_framework import serializers

from .models import Postcard


class PostcardSerializer(serializers.ModelSerializer):
    """
    Serializer that serializes postcard model.
    """
    author = serializers.ReadOnlyField(source='author.username')

    def to_representation(self, obj):
        """
        Override to shorten text by query param value.
        :param obj:
        :return:
        """
        result = super().to_representation(obj)
        text = self.shorten_text(obj)
        result.update({
            'text': text
        })
        return result

    def shorten_text(self, obj):
        """
        This methods allows to shorten text in the API by number of characters.
        :param obj:
        :return: shortened text
        """
        request = self.context.get("request")
        text = obj.text
        text_width = request.query_params.get('text_width')

        if request and text_width:
            if text_width.isdigit():
                text_width = int(text_width)
                text = textwrap.shorten(text, width=text_width, placeholder="...")
        return text

    class Meta:
        model = Postcard
        fields = '__all__'


class EmailSerializer(serializers.Serializer):
    """
    Serializer that validates the email.
    """
    email = serializers.EmailField()

    class Meta:
        fields = ('email',)
